<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class LoginPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required',
            'password'=>'required'
        ];
    }

    /**
     * Get the validation attributes that apply to response error request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'email'=>'e-mail',
            'password'=>'senha'
        ];
    }
}
