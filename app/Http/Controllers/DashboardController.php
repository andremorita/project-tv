<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Pagination\Paginator;
use App\Models\Imoveis;

class DashboardController extends Controller
{
    public function index()
    {
        $results = Imoveis::with(['tipos'])
        ->paginate(25);

        return view('admin.dashboard', [
            'results'=>$results
        ]);                 
    }
}
