<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

use App\Models\Imoveis;

class ImoveisController extends Controller
{
    public function gerenciar() 
    {   
        $results = Imoveis::with(['tipos'])
        ->paginate(25);

        return view('admin.imoveis.gerenciar', [
            'results'=>$results
        ]);   
    }
}
