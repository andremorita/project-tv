<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\LoginPostRequest;

use Auth;

class AutenticacaoController extends Controller
{
    public function login(LoginPostRequest $request) 
    {   
        $email = $request->email;
        $password = $request->password;
        $remember = $request->remember;

        $auth = Auth::attempt(['email' => $email, 'password' => $password], $remember);

        if ($auth) {
            return redirect()->route('admin.imoveis.gerenciar');
        } else {
            return redirect()
            ->route('admin.login')
            ->with(['messages'=>['Dados inválidos']]);
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('dashboard');
    }
}
