@extends('admin.main')



@section('content')
    <div class="dashboard">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="dashboard-left">
                        <div class="painel-left">
                            <h2>IMÓVEIS VENDIDOS</h2>
                            <strong>102</strong>
                        </div>
                        <div class="painel-left">
                            <h2>IMÓVEIS ALUGADOS</h2>
                            <strong>102</strong>
                        </div>
                        <div class="painel-left">
                            <h2>IMÓVEIS DISPONÍVEIS</h2>
                            <strong>102</strong>
                        </div> 
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="dashboard-middle">
                                <h2>Lorem Ipsum</h2>
                                <p>
                                    Duis nec tellus vel nibh maximus
                                    condimentum a facilisis mauris. Vivamus
                                    commodo sodales pellentesque. Etiam eget
                                    sem quis justo maximus pellentesque. Donec
                                    orci quam, pulvinar a porta sit amet, tempus
                                    in est. Proin tellus neque, condimentum vel
                                    turpis nec, sodales convallis arcu.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="feed-image">
                                <img src="{{asset('images/default.jpeg')}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-1 clock">
                    19:00
                </div>
                <div class="col-md-8 cidade-temperatura">
                    São Paulo - 18º
                </div>
                 <div class="col-md-3 logo">
                     LOGO EMPRESA
                </div>
            </div>
        </div>
    </div>
@endsection

