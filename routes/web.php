<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'admin', 'middleware'=>'auth:web'], function(){
    Route::get('admin/imoveis/gerenciar', ['as'=>'admin.imoveis.gerenciar','uses'=>'Admin\ImoveisController@gerenciar']);
    Route::get('admin/imoveis/{id}/editar', ['as'=>'admin.imoveis.editar','uses'=>'Admin\ImoveisController@editar']);
    Route::get('admin/imoveis/{id}/remover', ['as'=>'admin.autenticacao.{id}/remover','uses'=>'Admin\ImoveisController@{id}/remover']);
    Route::post('admin/imoveis/atualizar', ['as'=>'admin.autenticacao.atualizar','uses'=>'Admin\ImoveisController@atualizar']);
});

Route::get('admin/autenticacao/logout', ['as'=>'admin.autenticacao.logout','uses'=>'Admin\AutenticacaoController@logout']);
Route::post('admin/autenticacao/login', ['as'=>'admin.autenticacao.login','uses'=>'Admin\AutenticacaoController@login']);

Route::get('login', ['as'=>'admin.login','uses'=>function(){
	return view('admin.login');
}]);

Route::get('/', ['as'=>'dashboard','uses'=>'DashboardController@index']);

