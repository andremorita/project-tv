<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $data = [[
            'name' => 'André Morita',
            'email' => 'andreluizmorita@gmail.com',
            'password' => bcrypt('andremorita'),
        ], [
            'name' => 'User Teste1',
            'email' => 'user.teste1@gmail.com',
            'password' => bcrypt('userteste1'),
        ]];
        
        DB::table('users')->insert($data);
    }
}
